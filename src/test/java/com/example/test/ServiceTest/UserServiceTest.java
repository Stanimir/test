package com.example.test.ServiceTest;

import com.example.test.entity.User;
import com.example.test.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import java.util.List;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService service;

    @Before
    public void setUp() {
        service.evictCache();
    }

    @Test
    public void testFindAll() {

        User entity = new User();
        entity.setFirstName("test");
        entity.setSecondName("test");
        entity.setLastName("test");
        entity.setUsername("test");
        entity.setPassword("test");

        service.insert(entity);

        List<User> list = service.findAll();

        Assert.assertNotNull("failure - expected not null", list);
        Assert.assertEquals("failure - expected list size", 1, list.size());

    }

    @Test
    public void testFindOne() {

        User entity = new User();
        entity.setFirstName("test");
        entity.setSecondName("test");
        entity.setLastName("test");
        entity.setUsername("test");
        entity.setPassword("test");

        User insertedEntity = service.insert(entity);
        Assert.assertEquals("id is expected to be one",new Long(3),insertedEntity.getId());

        Long id = new Long(3);

        User result = service.getById(id);

        Assert.assertNotNull("failure - expected not null", result);
        Assert.assertEquals("failure - expected id attribute match", id,
                result.getId());

    }

    @Test
    public void testFindOneNotFound() {

        Long id = Long.MAX_VALUE;

        User entity = service.getById(id);

        Assert.assertNull("failure - expected null", entity);

    }

    @Test
    public void testCreate() {

        User entity = new User();
        entity.setFirstName("test");
        entity.setSecondName("test");
        entity.setLastName("test");
        entity.setUsername("test");
        entity.setPassword("test");

        User createdEntity = service.insert(entity);

        Assert.assertNotNull("failure - expected not null", createdEntity);
        Assert.assertNotNull("failure - expected id attribute not null",
                createdEntity.getId());
        Assert.assertEquals("failure - expected text attribute match", "test",
                createdEntity.getFirstName());

        List<User> list = service.findAll();

        Assert.assertEquals("failure - expected size", 1, list.size());

    }

    @Test
    public void testCreateWithId() {

        User entity1 = new User();
        entity1.setFirstName("test");
        entity1.setSecondName("test");
        entity1.setLastName("test");
        entity1.setUsername("test");
        entity1.setPassword("test");

        service.insert(entity1);

        Exception exception = null;
        Long id = new Long(1);

        User entity = new User();
        entity.setId(id);
        entity.setFirstName("test");
        entity.setSecondName("test");
        entity.setLastName("test");
        entity.setUsername("test");
        entity.setPassword("test");

        try {
            service.insert(entity);
        } catch (EntityExistsException e) {
            exception = e;
        }

        Assert.assertNotNull("failure - expected exception", exception);
        Assert.assertTrue("failure - expected EntityExistsException",
                exception instanceof EntityExistsException);

    }

    @Test
    public void testUpdate() {

        User entity1 = new User();
        entity1.setFirstName("test");
        entity1.setSecondName("test");
        entity1.setLastName("test");
        entity1.setUsername("test");
        entity1.setPassword("test");

        service.insert(entity1);

        Long id = new Long(6);

        User entity = service.getById(id);

        Assert.assertNotNull("failure - expected not null", entity);

        String updatedText = entity.getFirstName() + " test";
        entity.setFirstName(updatedText);
        User updatedEntity = service.update(entity);

        Assert.assertNotNull("failure - expected not null", updatedEntity);
        Assert.assertEquals("failure - expected id attribute match", id,
                updatedEntity.getId());
        Assert.assertEquals("failure - expected text attribute match",
                updatedText, updatedEntity.getFirstName());

    }

    @Test
    public void testUpdateNotFound() {

        Exception exception = null;

        User entity = new User();
        entity.setId(Long.MAX_VALUE);
        entity.setFirstName("test");
        entity.setSecondName("test");
        entity.setLastName("test");
        entity.setUsername("test");
        entity.setPassword("test");

        try {
            service.update(entity);
        } catch (NoResultException e) {
            exception = e;
        }

        Assert.assertNotNull("failure - expected exception", exception);
        Assert.assertTrue("failure - expected NoResultException",
                exception instanceof NoResultException);

    }

    @Test
    public void testDelete() {

        User entity1 = new User();
        entity1.setFirstName("test");
        entity1.setSecondName("test");
        entity1.setLastName("test");
        entity1.setUsername("test");
        entity1.setPassword("test");

        service.insert(entity1);

        Long id = new Long(5);

        User entity = service.getById(id);

        Assert.assertNotNull("failure - expected not null", entity);

        service.remove(id);

        List<User> list = service.findAll();

        Assert.assertEquals("failure - expected size", 0, list.size());

        User deletedEntity = service.getById(id);

        Assert.assertNull("failure - expected null", deletedEntity);

    }
}
