drop table if exists role;
drop table if exists user;
drop table if exists user_role;
create table role (id bigint not null auto_increment, role varchar(255) not null, primary key (id));
create table user (id bigint not null auto_increment, first_name varchar(255) not null, last_name varchar(255) not null, password varchar(255) not null, second_name varchar(255) not null, username varchar(255) not null, primary key (id));
create table user_role (user_id bigint not null, role_id bigint not null);
alter table user_role add constraint FKa68196081fvovjhkek5m97n3y foreign key (role_id) references role (id);
alter table user_role add constraint FK859n2jvi8ivhui0rl0esws6o foreign key (user_id) references user (id);