package com.example.test.controller;

import com.example.test.entity.Role;
import com.example.test.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/roles")
public class RoleController {

    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void signUp(@RequestBody Role role) {
        roleService.insert(role);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Role update(@RequestBody Role role) {
        return roleService.update(role);
    }


    @GetMapping(value = "/{id}")
    public @ResponseBody
    Role getOne(@PathVariable Long id) {
        return roleService.getById(id);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        roleService.remove(id);
    }


    @GetMapping
    public @ResponseBody
    List<Role> getAll() {
        return roleService.findAll();
    }
}
