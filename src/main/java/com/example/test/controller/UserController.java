package com.example.test.controller;

import com.example.test.JWT.JwtTokenUtils;
import com.example.test.entity.EmailFormat;
import com.example.test.entity.User;
import com.example.test.service.UserService;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.example.test.JWT.SecurityConstants.HEADER_STRING;
import static com.example.test.JWT.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private CacheManager cacheManager;

    private AuthenticationManager authenticationManager;

    private UserDetailsService userDetailsService;

    private JwtTokenUtils jwtTokenUtils;

    public UserController(UserService userService,
                          BCryptPasswordEncoder bCryptPasswordEncoder, CacheManager cacheManager,
                          @Qualifier("userDetailsServiceBean") UserDetailsService userDetailsService,
                          AuthenticationManager authenticationManager,
                          JwtTokenUtils jwtTokenUtils) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.cacheManager = cacheManager;
        this.userDetailsService = userDetailsService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtils = jwtTokenUtils;
    }


    @PostMapping(value = "/sign-up", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void signUp(@RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userService.insert(user);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    User update(@RequestBody User user) {
        return userService.update(user);
    }


    @GetMapping(value = "/{id}")
    public @ResponseBody
    User getOne(@PathVariable Long id) {
        return userService.getById(id);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        userService.remove(id);
    }


    @GetMapping
    public @ResponseBody
    List<User> getAll() {
        return userService.findAll();
    }

    @GetMapping(value = "/cache")
    public @ResponseBody
    String getCacheStats() {
        org.springframework.cache.Cache cache = cacheManager.getCache("USERS");
        Cache nativeCoffeeCache = (Cache) cache.getNativeCache();

        System.out.print(nativeCoffeeCache.stats().toString());
        return nativeCoffeeCache.stats().toString();
    }

    @PostMapping("/auth")
    public ResponseEntity createAuthenticationToken(@RequestBody User user, HttpServletResponse response) throws AuthenticationException {

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getUsername(),
                            user.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUsername());
        String token = jwtTokenUtils.generateToken(userDetails);
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PutMapping(value = "/mail", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    void sendMail(@RequestBody EmailFormat emailFormat) {
        userService.sendMail(emailFormat);
    }

}
