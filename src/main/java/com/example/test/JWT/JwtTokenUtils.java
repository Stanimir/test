package com.example.test.JWT;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.example.test.JWT.SecurityConstants.EXPIRATION_TIME;
import static com.example.test.JWT.SecurityConstants.SECRET;

@Component
public class JwtTokenUtils {
    public String getUsername(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getIssuedAtDate(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDate(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }


    private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver){
        final Claims claims = getAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaims(String token){
        return Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();
    }

    public String generateToken(UserDetails userDetails){
        Map<String, Object> claims = new HashMap<>();
        return generateToken(claims, userDetails.getUsername());
    }

    private String generateToken(Map<String, Object> claims, String subject){
        Date creationDate = new Date(System.currentTimeMillis());
        Date expirationDate = new Date(creationDate.getTime() + EXPIRATION_TIME);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(creationDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }

    public boolean isValid(String token, UserDetails userDetails){
        CustomUserDetails user = (CustomUserDetails) userDetails;

        return getUsername(token).equals(user.getUsername())
                && getIssuedAtDate(token).before(new Date(System.currentTimeMillis()))
                && getExpirationDate(token).after(new Date(System.currentTimeMillis()));
    }
}
