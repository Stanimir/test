package com.example.test.JWT;

        import com.example.test.service.UserDetailsServiceImpl;
        import org.springframework.beans.factory.annotation.Qualifier;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;

@Configuration
public class Configs {
    @Bean
    public JWTAuthenticationFilter authenticationTokenFilterBean() throws Exception {
        return new JWTAuthenticationFilter();
    }

    @Bean
    @Qualifier("userDetailsServiceBean")
    public UserDetailsServiceImpl userDetailsServiceBan() {
        return new UserDetailsServiceImpl();
    }
}
