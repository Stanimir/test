package com.example.test.service;

import com.example.test.entity.Role;
import com.example.test.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import java.util.List;

@Service
@CacheConfig(cacheNames = "ROLES")
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    JavaMailSender javaMailSender;

    @Cacheable(unless = "#result == null")
    public Role getByRoleName(String RoleName) {
        return roleRepository.getByRole(RoleName);
    }

    @CachePut(unless = "#result == null")
    public Role insert(Role Role) {
        if(Role.getId() != null && roleRepository.findOne(Role.getId()) != null)
            throw new EntityExistsException();

        roleRepository.save(Role);
        return roleRepository.getByRole(Role.getRole());
    }

    @Cacheable(unless = "#result == null")
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Cacheable(unless = "#result == null")
    public Role getById(final Long id) {
        return roleRepository.findOne(id);
    }

    @CacheEvict
    public void remove(Long id) {
        roleRepository.delete(id);
    }

    @CacheEvict
    public Role update(Role Role) {
        if(roleRepository.findOne(Role.getId()) == null)
            throw new NoResultException();

        roleRepository.save(Role);
        return Role;
    }
    @Caching(evict = @CacheEvict(value = "ROLES",allEntries = true))
    public void evictCache(){}
}
