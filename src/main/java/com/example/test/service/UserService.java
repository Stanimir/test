package com.example.test.service;

import com.example.test.entity.EmailFormat;
import com.example.test.entity.Role;
import com.example.test.entity.User;
import com.example.test.repository.RoleRepository;
import com.example.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.*;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service
@CacheConfig(cacheNames = "USERS")
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    RoleRepository roleRepository;

    @Value(value = "${spring.mail.username}")
    String mail;

    @Cacheable(unless = "#result == null")
    public User getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @CachePut(unless = "#result == null")
    public User insert(User user) {
        if(user.getId() != null && userRepository.findOne(user.getId()) != null)
            throw new EntityExistsException();
        if(user.getRoles() == null){
            List<Role> roles = new ArrayList<>();
            roles.add(roleRepository.getByRole("Regular"));
            user.setRoles(roles);
        }

        userRepository.save(user);
        return userRepository.findByUsername(user.getUsername());
    }

    @Cacheable(unless = "#result == null")
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Cacheable(unless = "#result == null")
    public User getById(final Long id) {
        return userRepository.findOne(id);
    }

    @CacheEvict
    public void remove(Long id) {
        userRepository.delete(id);
    }

    @CacheEvict
    public User update(User user) {
        if(userRepository.findOne(user.getId()) == null)
            throw new NoResultException();

        userRepository.save(user);
        return user;
    }
    @Caching(evict = @CacheEvict(value = "USERS",allEntries = true))
    public void evictCache(){}

    public void sendMail(EmailFormat emailFormat){

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(emailFormat.getEmail());
        simpleMailMessage.setFrom(mail);
        simpleMailMessage.setSubject("Testing sending some mails");
        simpleMailMessage.setText(emailFormat.getText());
        javaMailSender.send(simpleMailMessage);
    }
}
